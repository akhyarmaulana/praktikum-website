make ERD for anime website
table users
- id
- username
- email
- password
- name
- access_rule
- created_at
- updated_at

table animes
- id
- featured_image
- title
- slug
- total_episodes
- sinopsis
- premiered_at
- producers
- studios
- created_at
- updated_at

table animes_episodes
- id
- anime_id
- episode_no
- featured_image
- title
- description
- created_at
- updated_at

table anime_reviews
- id
- anime_id
- user_id
- message
- created_at
- updated_at

table news
- id
- featured_image
- banner_image
- title
- slug
- post_by_id
- body
- status
- publish_at
- created_at
- updated_at

table news_comments
- id
- news_id 
- user_id
- message
- created_at
- updated_at