<?php 

function is_admin()
{
    $id = session()->get('id');
    
    $user = new \App\Models\UsersModel;
    $userdata = $user->where("id",$id)->first();

    if (empty($userdata)) {
        return false;
    }

    if ($userdata->access_rule == "admin") {
        return true;   
    } else {
        return false;
    }
}