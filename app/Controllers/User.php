<?php
namespace App\Controllers;

use App\Models\UsersModel;

class User extends BaseController
{
    public function index()
    {
        if (!is_admin()) {
            session()->setFlashdata('error', 'Email & Password Salah');
            return redirect()->to(base_url('/admin/login'));
        }

        $userModel = new UsersModel();
        $data = [
            'title' => 'Admin Account',
            'user' => $userModel->get_user()
        ];

        echo view('admin/template/header', $data);
        echo view('admin/template/sidebar', $data);
        echo view('admin/view_user', $data);
        echo view('admin/template/footer');
    }
}
