<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class NewsController extends BaseController
{
    public function index()
    {
        $newsmodel = new \App\Models\News;
        $data['data'] = $newsmodel->limit(12)->findAll();
        $animemodel = new \App\Models\Animes;
        $data['trailers'] = $newsmodel->limit(12)->findAll();

        return view("anime/pages/news", $data);
    }

    public function detail($slug)
    {
        $newsmodel = new \App\Models\News;
        $data['data'] = $newsmodel->where("slug", $slug)->get()->getRow();
        $commentsmodel = new \App\Models\NewsComments;
        $data['comments'] = $commentsmodel->where("news_id", $data['data']->id)->findAll();

        return view("anime/pages/news-detail", $data);
    }
}
