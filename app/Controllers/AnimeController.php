<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class AnimeController extends BaseController
{
    public function index()
    {
        $animesmodel = new \App\Models\Animes;
        $data['animes'] = $animesmodel->limit(12)->findAll();
        $episodes = new \App\Models\AnimesEpisodes;
        $data['episodes'] = $episodes->limit(6)->findAll();

        return view("anime/pages/animes", $data);
    }

    public function detail($slug)
    {
        $animesmodel = new \App\Models\Animes;
        $data['data'] = $animesmodel->where("slug", $slug)->get()->getRow();
        $reviewmodel = new \App\Models\AnimesReviews;
        $data['review'] = $reviewmodel->where("anime_id", $data['data']->id)->findAll();

        return view("anime/pages/animes-detail", $data);
    }
}
