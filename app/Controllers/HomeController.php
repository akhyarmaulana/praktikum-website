<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class HomeController extends BaseController
{
    public function index()
    {
        $animesmodel = new \App\Models\Animes;
        $newsmodel = new \App\Models\News;
        $episodesmodel = new \App\Models\AnimesEpisodes;

        $recent_episode = $episodesmodel->orderBy('id', 'desc')->limit(4)->findAll();
        foreach ($recent_episode as $k => $v) {
            $anime = $animesmodel->where("id", $v['anime_id'])->first();
            $recent_episode[$k]["anime_name"] = $anime['title'];
        }
        
        $data['best_anime'] = $animesmodel->limit(4)->findAll();
        $data['recent_news'] = $newsmodel->orderBy('id', 'desc')->limit(3)->findAll();
        $data['recent_episode'] = $recent_episode;
        $data['recent_trailer'] = $animesmodel->orderBy('id', 'desc')->limit(4)->findAll();

        return view("anime/pages/home", $data);
    }
}
