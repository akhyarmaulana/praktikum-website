<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Files\File;

class UserController extends BaseController
{
    protected $title = "User Management";
    protected $module = "user";
    protected $model = "\\App\\Models\\UsersModel";
    protected $table = [
        ['name' => 'username', 'label' => 'Username'],
        ['name' => 'email', 'label' => 'Email'],
        ['name' => 'name', 'label' => 'Name'],
        ['name' => 'access_rule', 'label' => 'Access Rule'],
    ];
    protected $form = [
        ['name' => 'username', 'label' => 'Username', 'type' => 'text'],
        ['name' => 'email', 'label' => 'Email', 'type' => 'text'],
        ['name' => 'name', 'label' => 'Name', 'type' => 'text'],
        ['name' => 'access_rule', 'label' => 'Access Rule', 'type' => 'select', 'options' => [
            ['value' => 'admin', 'label' => 'Admin'],
            ['value' => 'users', 'label' => 'User'],
        ]],
    ];
    

    public function index()
    {
        $model = new $this->model();
        $data = [
            'title' => $this->title,
            'data' => $model->findAll(),
            'module' => $this->module,
            'table' => $this->table,
        ];
        // var_dump($data); exit();
        echo view('admin/template/header', $data);
        echo view('admin/template/sidebar', $data);
        echo view('admin/cms/view_table', $data);
        echo view('admin/template/footer');
    }

    public function form($mode , $reff_id = null)
    {
        $data = [
            'mode' => $mode,
            'reff_id' => $reff_id,
            'title' => $this->title,
            'module' => $this->module,
            'form' => $this->form,
        ];

        if ($mode == "edit") {
            $model = new $this->model();
            $data['data'] = $model->find($reff_id);
        }

        echo view('admin/template/header', $data);
        echo view('admin/template/sidebar', $data);
        echo view('admin/cms/view_form', $data);
        echo view('admin/template/footer');
    }

    public function formAction($mode, $reff_id = null)
    {
        error_reporting(E_ALL & ~E_NOTICE);

        $insert = [];
        foreach ($this->form as $f) {
            if ($f['type'] == "file") {
                $path = $this->upload($f['name']);
                if (!empty($path)) {
                    $insert[$f['name']] = $path;
                }
                
            } else {
                $insert[$f['name']] = $this->request->getVar($f['name']);
            }
        }

        $model = new $this->model();
        if ($mode == "add") {
            $model->insert($insert);
        } else if ($mode == "edit") {
            $model->update($reff_id, $insert);
        }
        
        // return redirect()->to('/admin/' . $this->module);
        $this->forceredirect('/admin/' . $this->module);
    }

    public function deleteAction($id)
    {
        try {
            $datamodel = new $this->model();
            $datamodel->where("id", $id)->delete();
        } catch (\Exception $e) {
            // Log or handle the exception
            echo $r->getMessage();
        }
        return redirect()->to('admin/' . $this->module);
    }


    public function upload($field_name)
    {
        $file = $this->request->getFile($field_name);
        if ($file->isValid() && !$file->hasMoved()) {
            $newName = $file->getRandomName();
            $file->move(__DIR__ .'/../../../public/uploads', $newName);

            // Return the file path
            $filePath = 'uploads/' . $newName;
            return $filePath;
        } 
        return null;
    }

    public function forceredirect($uri)
    {
        echo '<script>window.location = "'.$uri.'"</script>'; exit();
    }
}
