<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class EpisodesController extends BaseController
{
    
    protected $title = "Episodes Management";
    protected $module = "episodes";
    protected $model = "\\App\\Models\\AnimesEpisodes";
    protected $table = [
        ['name' => 'featured_image', 'label' => 'Featured Images', 'image' => true],
        ['name' => 'anime_name', 'label' => 'Related Anime'],
        ['name' => 'title', 'label' => 'Title'],
        ['name' => 'episode_no', 'label' => 'Episode No'],
    ];
    protected $form = [
        ['name' => 'featured_image', 'label' => 'Featured Images', 'type' => 'file'],
        ['name' => 'anime_id', 'label' => 'Anime', 'type' => 'select', 'options' => []],
        ['name' => 'title', 'label' => 'Title', 'type' => 'text'],
        ['name' => 'episode_no', 'label' => 'Episode No', 'type' => 'text'],  
    ];
    
    public function index()
    {
        $model = new $this->model();
        $data_mode = $model->all();
        foreach ($data_mode as $k => $v) {
            $animes = new \App\Models\Animes;
            $anime = $animes->find($v['anime_id']);
            $data_mode[$k]["anime_name"] = $anime['title'];
        }

        $data = [
            'title' => $this->title,
            'data' => $data_mode,
            'module' => $this->module,
            'table' => $this->table,
        ];
        // var_dump($data); exit();
        echo view('admin/template/header', $data);
        echo view('admin/template/sidebar', $data);
        echo view('admin/cms/view_table', $data);
        echo view('admin/template/footer');
    }

    public function form($mode , $reff_id = null)
    {
        //options
        $anime_options = [];
        $animes = new \App\Models\Animes;
        $animes = $animes->findAll();
        
        foreach( $animes as $v) {
            $anime_options[] = [
                'value' => $v['id'],
                'label' => $v['title']
            ];
        }

        /* form modification */
        $this->form[1]['options'] = $anime_options;

        $data = [
            'mode' => $mode,
            'reff_id' => $reff_id,
            'title' => $this->title,
            'module' => $this->module,
            'form' => $this->form,
        ];

        if ($mode == "edit") {
            $model = new $this->model();
            $data['data'] = $model->find($reff_id);
        }

        echo view('admin/template/header', $data);
        echo view('admin/template/sidebar', $data);
        echo view('admin/cms/view_form', $data);
        echo view('admin/template/footer');
    }

    public function formAction($mode, $reff_id = null)
    {
        error_reporting(E_ALL & ~E_NOTICE);

        $insert = [];
        foreach ($this->form as $f) {
            if ($f['type'] == "file") {
                $path = $this->upload($f['name']);
                if (!empty($path)) {
                    $insert[$f['name']] = $path;
                }
                
            } else {
                $insert[$f['name']] = $this->request->getVar($f['name']);
            }
        }

        $model = new $this->model();
        if ($mode == "add") {
            $model->insert($insert);
        } else if ($mode == "edit") {
            $model->update($reff_id, $insert);
        }
        

        // return redirect()->to('/admin/' . $this->module);
        $this->forceredirect('/admin/' . $this->module);
    }

    public function deleteAction($id)
    {
        try {
            $datamodel = new $this->model();
            $datamodel->where("id", $id)->delete();
        } catch (\Exception $e) {
            // Log or handle the exception
            echo $r->getMessage();
        }
        return redirect()->to('admin/' . $this->module);
    }


    public function upload($field_name)
    {
        $file = $this->request->getFile($field_name);
        if ($file->isValid() && !$file->hasMoved()) {
            $newName = $file->getRandomName();
            $file->move(__DIR__ .'/../../../public/uploads', $newName);

            // Return the file path
            $filePath = 'uploads/' . $newName;
            return $filePath;
        } 
        return null;
    }

    public function forceredirect($uri)
    {
        echo '<script>window.location = "'.$uri.'"</script>'; exit();
    }
    
}
