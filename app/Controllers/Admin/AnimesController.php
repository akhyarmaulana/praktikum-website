<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Files\File;

class AnimesController extends BaseController
{
    protected $title = "Animes Management";
    protected $module = "animes";
    protected $model = "\\App\\Models\\Animes";
    protected $table = [
        ['name' => 'featured_image', 'label' => 'Featured Images', 'image' => true],
        ['name' => 'title', 'label' => 'Title'],
        ['name' => 'total_episode', 'label' => 'Total Episode'],
        ['name' => 'premiered_at', 'label' => 'Premierd At'],
        ['name' => 'producers', 'label' => 'Producers'],
        ['name' => 'studios', 'label' => 'Studios'],
    ];
    protected $form = [
        ['name' => 'featured_image', 'label' => 'Featured Images', 'type' => 'file'],
        ['name' => 'title', 'label' => 'Title', 'type' => 'text'],
        ['name' => 'slug', 'label' => 'Slug', 'type' => 'text'],
        ['name' => 'total_episode', 'label' => 'Total Episode', 'type' => 'number'],
        ['name' => 'premiered_at', 'label' => 'Premierd At', 'type' => 'select', 'options' => [
            ['value' => 'fall 2023', 'label' => 'Fall 2023'],
            ['value' => 'winter 2023', 'label' => 'Winter 2023'],
            ['value' => 'spring 2024', 'label' => 'Spring 2024'],
            ['value' => 'summer 2024', 'label' => 'Summer 2024'],
            ['value' => 'fall 2024', 'label' => 'Fall 2024'],
            ['value' => 'winter 2024', 'label' => 'Winter 2024'],
        ]],
        ['name' => 'producers', 'label' => 'Producers', 'type' => 'textarea'],
        ['name' => 'studios', 'label' => 'Studios', 'type' => 'textarea'],
        ['name' => 'genres', 'label' => 'Genres', 'type' => 'textarea'],
        ['name' => 'sinopsis', 'label' => 'Synopsis', 'type' => 'textarea'],
    ];
    

    public function index()
    {
        $model = new $this->model();
        $data = [
            'title' => $this->title,
            'data' => $model->all(),
            'module' => $this->module,
            'table' => $this->table,
        ];
        // var_dump($data); exit();
        echo view('admin/template/header', $data);
        echo view('admin/template/sidebar', $data);
        echo view('admin/cms/view_table', $data);
        echo view('admin/template/footer');
    }

    public function form($mode , $reff_id = null)
    {
        $data = [
            'mode' => $mode,
            'reff_id' => $reff_id,
            'title' => $this->title,
            'module' => $this->module,
            'form' => $this->form,
        ];

        if ($mode == "edit") {
            $model = new $this->model();
            $data['data'] = $model->find($reff_id);
        }

        echo view('admin/template/header', $data);
        echo view('admin/template/sidebar', $data);
        echo view('admin/cms/view_form', $data);
        echo view('admin/template/footer');
    }

    public function formAction($mode, $reff_id = null)
    {
        error_reporting(E_ALL & ~E_NOTICE);

        $insert = [];
        foreach ($this->form as $f) {
            if ($f['type'] == "file") {
                $path = $this->upload($f['name']);
                if (!empty($path)) {
                    $insert[$f['name']] = $path;
                }
                
            } else {
                $insert[$f['name']] = $this->request->getVar($f['name']);
            }
        }

        $model = new $this->model();
        if ($mode == "add") {
            $model->insert($insert);
        } else if ($mode == "edit") {
            $model->update($reff_id, $insert);
        }
        
        $this->forceredirect('/admin/' . $this->module);
    }

    public function deleteAction($id)
    {
        try {
            $datamodel = new $this->model();
            $datamodel->where("id", $id)->delete();
        } catch (\Exception $e) {
            // Log or handle the exception
            echo $r->getMessage();
        }
        return redirect()->to('admin/' . $this->module);
    }


    public function upload($field_name)
    {
        $file = $this->request->getFile($field_name);
        if ($file->isValid() && !$file->hasMoved()) {
            $newName = $file->getRandomName();
            $file->move(__DIR__ .'/../../../public/uploads', $newName);

            // Return the file path
            $filePath = 'uploads/' . $newName;
            return $filePath;
        } 
        return null;
    }

    public function forceredirect($uri)
    {
        echo '<script>window.location = "'.$uri.'"</script>'; exit();
    }
}
