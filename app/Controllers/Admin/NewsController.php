<?php

namespace App\Controllers\Admin;

use App\Controllers\BaseController;
use CodeIgniter\HTTP\ResponseInterface;

class NewsController extends BaseController
{
    protected $title = "News Management";
    protected $module = "news";
    protected $model = "\\App\\Models\\News";
    protected $table = [
        ['name' => 'featured_image', 'label' => 'Featured Image', 'image' => true],
        ['name' => 'banner_image', 'label' => 'Banner Image', 'image' => true],
        ['name' => 'title', 'label' => 'Title'],
        ['name' => 'post_by_name', 'label' => 'Posted By'],
    ];
    protected $form = [
        ['name' => 'featured_image', 'label' => 'Featured Images', 'type' => 'file'],
        ['name' => 'banner_image', 'label' => 'Banner Images', 'type' => 'file'],
        ['name' => 'title', 'label' => 'Title', 'type' => 'text'],
        ['name' => 'slug', 'label' => 'Slug', 'type' => 'text'],
        ['name' => 'post_by_name', 'label' => 'Post By', 'type' => 'text'],
        ['name' => 'body', 'label' => 'Article', 'type' => 'summernote', 
            'function_store' => 'base64_encode', 
            'function_get' => 'base64_decode'
        ],
    ];
    public function index()
    {
        $model = new $this->model();

        $data = [
            'title' => $this->title,
            'data' => $model->findAll(),
            'module' => $this->module,
            'table' => $this->table,
        ];
        // var_dump($data); exit();
        echo view('admin/template/header', $data);
        echo view('admin/template/sidebar', $data);
        echo view('admin/cms/view_table', $data);
        echo view('admin/template/footer');
    }

    public function form($mode , $reff_id = null)
    {
        $data = [
            'mode' => $mode,
            'reff_id' => $reff_id,
            'title' => $this->title,
            'module' => $this->module,
            'form' => $this->form,
        ];

        if ($mode == "edit") {
            $model = new $this->model();
            $data['data'] = $model->find($reff_id);
        }

        echo view('admin/template/header', $data);
        echo view('admin/template/sidebar', $data);
        echo view('admin/cms/view_form', $data);
        echo view('admin/template/footer');
    }

    public function formAction($mode, $reff_id = null)
    {
        $insert = [];
        foreach ($this->form as $f) {
            if ($f['type'] == "file") {
                $path = $this->upload($f['name']);
                if (!empty($path)) {
                    $insert[$f['name']] = $path;
                }
            } else {
                if (!empty($f['function_store'])) {
                    $insert[$f['name']] = $f['function_store']($this->request->getVar($f['name'])) ;
                } else {
                    $insert[$f['name']] = $this->request->getVar($f['name']);
                }
            }
        }

        $model = new $this->model();
        if ($mode == "add") {
            $model->insert($insert);
        } else if ($mode == "edit") {
            $model->update($reff_id, $insert);
        }

        $this->forceredirect('/admin/' . $this->module);
    }

    public function deleteAction($id)
    {
        try {
            $datamodel = new $this->model();
            $datamodel->where("id", $id)->delete();
        } catch (\Exception $e) {
            // Log or handle the exception
            echo $r->getMessage();
        }
        return redirect()->to('admin/' . $this->module);
    }


    public function upload($field_name)
    {
        $file = $this->request->getFile($field_name);
        if ($file->isValid() && !$file->hasMoved()) {
            $newName = $file->getRandomName();
            $file->move(__DIR__ .'/../../../public/uploads', $newName);

            // Return the file path
            $filePath = 'uploads/' . $newName;
            return $filePath;
        } 
        return null;
    }

    public function forceredirect($uri)
    {
        echo '<script>window.location = "'.$uri.'"</script>'; exit();
    }
}
