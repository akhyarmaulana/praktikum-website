<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */

/* 
 *
 * lat CI 2 
*/
// $routes->get('/', 'Home::index');
// $routes->get('/admin/register', 'Register::index');
// $routes->post('/admin/register/process', 'Register::process');

/* admin route */
$routes->get('/admin/login', 'Login::index');
$routes->post('/admin/login/process', 'Login::process');
// $routes->get('/admin/user', 'User::index');

/* users */
$routes->get('/admin/user', '\App\Controllers\Admin\UserController::index');
$routes->get('/admin/user/delete/(:any)', '\App\Controllers\Admin\UserController::deleteAction/$1');
$routes->get('/admin/user/(:any)', '\App\Controllers\Admin\UserController::form/$1');
$routes->get('/admin/user/(:any)/(:num)', '\App\Controllers\Admin\UserController::form/$1/$2');
$routes->post('/admin/user/(:any)', '\App\Controllers\Admin\UserController::formAction/$1');
$routes->post('/admin/user/(:any)/(:num)', '\App\Controllers\Admin\UserController::formAction/$1/$2');


/* animes */
$routes->get('/admin/animes', '\App\Controllers\Admin\AnimesController::index');
$routes->get('/admin/animes/delete/(:any)', '\App\Controllers\Admin\AnimesController::deleteAction/$1');
$routes->get('/admin/animes/(:any)', '\App\Controllers\Admin\AnimesController::form/$1');
$routes->get('/admin/animes/(:any)/(:num)', '\App\Controllers\Admin\AnimesController::form/$1/$2');
$routes->post('/admin/animes/(:any)', '\App\Controllers\Admin\AnimesController::formAction/$1');
$routes->post('/admin/animes/(:any)/(:num)', '\App\Controllers\Admin\AnimesController::formAction/$1/$2');

/* news */
$routes->get('/admin/episodes', '\App\Controllers\Admin\EpisodesController::index');
$routes->get('/admin/episodes/delete/(:any)', '\App\Controllers\Admin\EpisodesController::deleteAction/$1');
$routes->get('/admin/episodes/(:any)', '\App\Controllers\Admin\EpisodesController::form/$1');
$routes->get('/admin/episodes/(:any)/(:num)', '\App\Controllers\Admin\EpisodesController::form/$1/$2');
$routes->post('/admin/episodes/(:any)', '\App\Controllers\Admin\EpisodesController::formAction/$1');
$routes->post('/admin/episodes/(:any)/(:num)', '\App\Controllers\Admin\EpisodesController::formAction/$1/$2');

/* news */
$routes->get('/admin/news', '\App\Controllers\Admin\NewsController::index');
$routes->get('/admin/news/delete/(:any)', '\App\Controllers\Admin\NewsController::deleteAction/$1');
$routes->get('/admin/news/(:any)', '\App\Controllers\Admin\NewsController::form/$1');
$routes->get('/admin/news/(:any)/(:num)', '\App\Controllers\Admin\NewsController::form/$1/$2');
$routes->post('/admin/news/(:any)', '\App\Controllers\Admin\NewsController::formAction/$1');
$routes->post('/admin/news/(:any)/(:num)', '\App\Controllers\Admin\NewsController::formAction/$1/$2');

$routes->get('/', 'HomeController::index');
$routes->get('/anime', 'AnimeController::index');
$routes->get('/anime/detail/(:any)', 'AnimeController::detail/$1');
$routes->get('/news', 'NewsController::index');
$routes->get('/news/detail/(:any)', 'NewsController::detail/$1');
$routes->get('/genre', 'GenresControler::index');


$routes->get('/register', 'AuthController::register');
$routes->post('/register', 'AuthController::doRegister');
$routes->get('/login', 'AuthController::login');
$routes->post('/login', 'AuthController::goLogin');
