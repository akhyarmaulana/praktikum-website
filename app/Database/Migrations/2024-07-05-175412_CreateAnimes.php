<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class CreateAnimes extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'id' => [
                'type' => 'INT',
                'constraint' => 11,
                'null' => false,
                'auto_increment' => true
            ],
            'featured_image' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'title' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => false,
            ],
            'slug' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => false,
            ],
            'total_episode' => [
                'type' => 'INT',
                'constraint' => 11,
                'null' => true,
            ],
            'sinopsis' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'premiered_at' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'null' => true,
            ],
            'producers' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'studios' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'genres' => [
                'type' => 'TEXT',
                'null' => true,
            ],
            'created_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
            'updated_at' => [
                'type' => 'DATETIME',
                'null' => true,
            ],
        ]);
        $this->forge->addPrimaryKey('id');
        $this->forge->createTable('animes');
    }

    public function down()
    {
        $this->forge->dropTable('animes');
    }
}
