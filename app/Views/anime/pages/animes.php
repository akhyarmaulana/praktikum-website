<?= $this->extend('anime/includes/template') ?>

<?= $this->section('content') ?>


<section class="container px-0 bg-light">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 pt-5">
                <form class="d-flex pl-3" role="search" data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1000">
                    <div class="input-group">
                        <input name="search" type="text" class="form-control no-round" placeholder="Search anime" aria-label="Recipient's username" aria-describedby="search-action" value="<?= !empty($_GET['search']) ? $_GET['search'] : "" ?>">
                        <button class="btn btn-primary no-round" type="button" id="search-action"><i class="fa fa-search"></i> Search</button>
                    </div>
                </form>
            </div>
            <div class="col-12 text-center">
                <small data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1200"><i>Show 12 best result from 24.232 key words founds</i></small>
            </div>
        </div>
    </section>

    
    <section class="container featured-anime bg-light px-0">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                <div class="best-section-container bg-light">
                    <div class="row">
                        <div class="col-12">
                            <div class="featured-category-container mb-4 pt-3">
                                <h2 class="the-best-title" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="100">
                                    <span class="pt-3">All Animes</span>
                                </h2>
                                <ul class="the-best-of-category">
                                    <li>
                                        <a href="<?= base_url('anime') ?>?premiered_at=spring 2024" class="btn btn-sm btn-primary no-round" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="100">Spring 2024</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('anime') ?>?premiered_at=winter 2023" class="btn btn-sm btn-outline-primary no-round" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="200">Winter 2023</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('anime') ?>?premiered_at=fall 2023" class="btn btn-sm btn-outline-primary no-round" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="300">Fall 2023</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url('anime') ?>?premiered_at=summer 2023" class="btn btn-sm btn-outline-primary no-round" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="400">Summer 2023</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                    <?php foreach ($animes as $anime) : ?>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                            <?= view('anime/includes/vertical-anime-card', $anime) ?>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                <div class="latest-episodes-container bg-light px-5 px-sm-5 px-md-1 px-lg-1">
                    <div class="row">
                        <div class="col-12">
                            <h2 data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="900">Lates Episodes</h2>
                        </div>
                    </div>
                    <div class="row small-horizontal-card" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="1000">
                        <div class="col-4">
                            <a href="anime-detail.html"><img src="assets/img/frieren-small.jpeg" alt="card title"></a>
                        </div>
                        <div class="col-8">
                            <h1><a href="anime-detail.html">Sosou no Frieren</a></h1>
                            <h3>Episode 27, Fighting Double</h3>
                            <span>1,012 views</span>
                        </div>
                    </div>
                    <div class="row small-horizontal-card" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="1100">
                        <div class="col-4">
                            <a href="anime-detail.html"><img src="assets/img/frieren-small.jpeg" alt="card title"></a>
                        </div>
                        <div class="col-8">
                            <h1><a href="anime-detail.html">Sosou no Frieren</a></h1>
                            <h3>Episode 27, Fighting Double</h3>
                            <span>1,012 views</span>
                        </div>
                    </div>
                    <div class="row small-horizontal-card" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="1200">
                        <div class="col-4">
                            <a href="anime-detail.html"><img src="assets/img/frieren-small.jpeg" alt="card title"></a>
                        </div>
                        <div class="col-8">
                            <h1><a href="anime-detail.html">Sosou no Frieren</a></h1>
                            <h3>Episode 27, Fighting Double</h3>
                            <span>1,012 views</span>
                        </div>
                    </div>
                    <div class="row small-horizontal-card" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="1300">
                        <div class="col-4">
                            <a href="anime-detail.html"><img src="assets/img/frieren-small.jpeg" alt="card title"></a>
                        </div>
                        <div class="col-8">
                            <h1><a href="anime-detail.html">Sosou no Frieren</a></h1>
                            <h3>Episode 27, Fighting Double</h3>
                            <span>1,012 views</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


<?= $this->endSection() ?>