<?= $this->extend('anime/includes/template') ?>

<?= $this->section('content') ?>

<section class="container detail-news bg-light px-0 pt-2">
        <div class="row">
            <div class="col-12 px-5">
                <img src="<?=base_url($data->banner_image)?>" alt="Article Title" class="w-100 article-banner" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="400">
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-7 col-lg-7 px-5">
                <h1 data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="500"><?=$data->title?></h1>
                <span data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="600">Post by <a href="" class="text-primary" ><?=$data->post_by_name?></a> |</span><span data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="700">Post At <a href="" class="text-primary"><?= date("d/m/Y H:i", strtotime($data->created_at)) ?></a></span> 
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-7 col-lg-7 px-5 py-3">
                

                <?= base64_decode($data->body) ?>   

                
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-7 col-lg-7 px-5">
                <h3>Comments</h3>
                <hr>
                <?php foreach ($comments as $v) : ?>
                <div class="anime-review-card" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="800">
                    <div class="row">
                        <div class="col-12">
                            <img src="<?= base_url($v->profile_picture) ?>" alt="User picture" class="user-picture-comment">
                            <h4><?= $v->name ?></h4>
                            <span class="text-secondary">
                            <?= time_ago($v->create_at) ?>
                            </span>
                        </div>
                        
                        <div class="col-12 py-2">
                            <p><?= $v->message ?></p>
                            <hr>
                        </div>
                        
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

<?= $this->endSection() ?>