<?= $this->extend('anime/includes/template') ?>

<?= $this->section('content') ?>


<section class="container px-0 bg-light">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-6 col-lg-6 pt-5">
                <form class="d-flex pl-3" role="search" data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1000">
                    <div class="input-group">
                        <input type="text" class="form-control no-round" placeholder="Search anime" aria-label="Recipient's username" aria-describedby="search-action" value="Frieren">
                        <button class="btn btn-primary no-round" type="button" id="search-action"><i class="fa fa-search"></i> Search</button>
                    </div>
                </form>
            </div>
            <div class="col-12 text-center">
                <small data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1200"><i>Show 6 best result from 24.232 key words founds</i></small>
            </div>
        </div>
    </section>

    
    <section class="container new-and-trailer-container bg-light px-0 pt-3 pb-4">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-8 col-lg-8">
                <div class="new-container px-5">
                    <div class="row">
                        <div class="col-9">
                            <h2 data-aos="fade-left" data-aos-duration="4000" data-aos-delay="100">Anime News</h2>
                        </div>
                    </div>
                    
                    <?php foreach ($data as $news) : ?>
                    <div class="row big-horizontal-card" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="300">
                         <div class="col-4">
                            <img src="<?=$news['featured_image']?>" alt="News Title Here">
                         </div>
                         <div class="col-8">
                            <small>Posted on <?= $news['created_at'] ?></small>
                            <h3><?=$news['title']?></h3>
                            <p>
                            <?= substr(strip_tags(base64_decode($news['body'])),0,100) ?>    
                            .... </p>
                            <a href="<?=base_url('news/detail/' . $news['slug'])?>" class="text-primary">Continue reading...</a>
                            <a href="<?=base_url('news/detail/' . $news['slug'])?>" class="text-secondary float-right"><i class="fa fa-comment"></i> <?=rand(11,99)?> Comments</a>
                         </div>
                    </div>

                    <?php endforeach; ?>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                <div class="latest-trailer-container px-5 px-sm-5 px-md-0 px-md-0">
                    <div class="row">
                        <div class="col-7 col-sm-7 col-md-6 col-lg-6">
                            <h2 data-aos="fade-up" data-aos-duration="4000" data-aos-delay="100">Recent Trailer</h2>
                        </div>
                        <div class="col-5 col-sm-5 col-md-6 col-md-6 px-0 px-sm-0 px-md-5 px-md-5">
                            <a href="trailer.html" class="btn btn-sm btn-outline-primary float-right no-round" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="100">View More</a>
                        </div>
                    </div>
                    <div class="row small-horizontal-card-video" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="100">
                        <div class="col-5">
                            <a class="thumb d-block" href="trailer-detail.html" style="background-image: url('assets/img/frieren-list-small.png');">
                                <img src="assets/img/youtube-play-transparent.png" alt="trailer card">
                            </a>
                        </div>
                        <div class="col-7">
                            <a href="trailer-detail.html"><h3>Sosou no Frieren</h3></a>
                            <p>Episode 27, Fighting Double</p>
                            <span>1,012 views</span>
                            <a href="trailer-detail.html" class="text-primary">24 Comments</a>
                        </div>
                    </div>
                    <div class="row small-horizontal-card-video" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="200">
                        <div class="col-5">
                            <a class="thumb d-block" href="trailer-detail.html" style="background-image: url('assets/img/frieren-list-small.png');">
                                <img src="assets/img/youtube-play-transparent.png" alt="trailer card">
                            </a>
                        </div>
                        <div class="col-7">
                            <a href="trailer-detail.html"><h3>Sosou no Frieren</h3></a>
                            <p>Episode 27, Fighting Double</p>
                            <span>1,012 views</span>
                            <a href="trailer-detail.html" class="text-primary">24 Comments</a>
                        </div>
                    </div>
                    <div class="row small-horizontal-card-video" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="300">
                        <div class="col-5">
                            <a class="thumb d-block" href="trailer-detail.html" style="background-image: url('assets/img/frieren-list-small.png');">
                                <img src="assets/img/youtube-play-transparent.png" alt="trailer card">
                            </a>
                        </div>
                        <div class="col-7">
                            <a href="trailer-detail.html"><h3>Sosou no Frieren</h3></a>
                            <p>Episode 27, Fighting Double</p>
                            <span>1,012 views</span>
                            <a href="trailer-detail.html" class="text-primary">24 Comments</a>
                        </div>
                    </div>
                    <div class="row small-horizontal-card-video" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="400">
                        <div class="col-5">
                            <a class="thumb d-block" href="trailer-detail.html" style="background-image: url('assets/img/frieren-list-small.png');">
                                <img src="assets/img/youtube-play-transparent.png" alt="trailer card">
                            </a>
                        </div>
                        <div class="col-7">
                            <a href="trailer-detail.html"><h3>Sosou no Frieren</h3></a>
                            <p>Episode 27, Fighting Double</p>
                            <span>1,012 views</span>
                            <a href="trailer-detail.html" class="text-primary">24 Comments</a>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </section>


<?= $this->endSection() ?>