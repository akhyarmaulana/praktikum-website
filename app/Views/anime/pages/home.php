<?= $this->extend('anime/includes/template') ?>

<?= $this->section('content') ?>
<section class="container banner-container px-0" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1200">
    <div class="row">
        <div class="col-12">
            <div id="home_carousel" class="carousel slide" data-bs-ride="carousel">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#home_carousel" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#home_carousel" data-bs-slide-to="1" aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#home_carousel" data-bs-slide-to="2" aria-label="Slide 3"></button>
                </div>
                <div class="carousel-inner">
                <!-- frierend start -->
                    <div class="carousel-item active">
                    <div class="main-banner-item bg-secondary" style="background-image: url('assets/img/banner-frieren.jpeg');">
                        <div class="container white-gradient-left">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-5">
                                    <div class="small-genres" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1300">ADVENTURE, FANTASY</div>
                                    <h1 class="main-title" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1400">SOSOU NO FRIEREN</h1>
                                    <h3 class="sub-title" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1500">LAST EPISODE OF THE SESSION TODAY</h3>
                                    <p>Frieren faild to get 1st grade magic badge. Now is up to Fren to get the badge. Only the best will be honourd 1st grage magic by Sirie</p>
                                    <a class="btn btn-danger no-round" data-bs-toggle="modal" data-bs-target="#trailer_modal" onclick="opentrailer('https\:\/\/www.youtube.com/embed/Y3l9RTUiYy0?si=AO44dwm6_txYAiYT')" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1600">
                                        <i class="fa fa-youtube"></i> Watch Trailer
                                    </a>
                                </div>
                            </div>
                            <div class="row d-block d-sm-none d-md-none d-lg-none">
                                <div class="col-12 px-5">
                                    <a class="btn btn-dark no-round w-100" type="button" data-bs-target="#home_carousel" data-bs-slide="prev" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1700">
                                        <i class="fa fa-chevron-left"></i> I Got Reincarnated as a Slime
                                    </a>
                                </div>
                                <div class="col-12 px-5">
                                    <a class="btn btn-primary no-round w-100 text-left" type="button" data-bs-target="#home_carousel" data-bs-slide="next"data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1800">
                                        Chainsaw Man <i class="fa fa-chevron-right"></i> 
                                    </a>
                                </div>
                            </div>
                            <div class="row d-none d-sm-block d-md-block d-lg-block">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-5">
                                    <div class="row">
                                        <div class="col-6 col-md-5">
                                            <a class="back-action d-block" style="background-image: url('assets/img/rimuru-el-slilme-small.jpeg');" type="button" data-bs-target="#home_carousel" data-bs-slide="prev" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1700">
                                                <div class="row dark-overlay py-3">
                                                    <div class="col-2 carousel-action-icon">
                                                        <i class="fa fa-chevron-left"></i>
                                                    </div>
                                                    <div class="col-10 text-left carousel-action-text pr-2">
                                                        <span>I Got Reincarnated as a Slime</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-5">
                                            <a class="back-action d-block" style="background-image: url('assets/img/cain-saw-man-small.jpeg');" type="button" data-bs-target="#home_carousel" data-bs-slide="next" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1800">
                                                <div class="row blue-overlay py-3">
                                                    <div class="col-10 carousel-action-text pl-2">
                                                        <span class="text-left pr-0 pl-2">Chainsaw Man<br>チェンソーマン</span>
                                                    </div>
                                                    <div class="col-2 carousel-action-icon">
                                                        <i class="fa fa-chevron-right"></i>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    </div>
                <!-- frierend end -->
                <!-- cainsawman start -->
                    <div class="carousel-item">
                    <div class="main-banner-item bg-secondary" style="background-image: url('assets/img/banner-chainsawman.jpeg');">
                        <div class="container white-gradient-left">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-5">
                                    <div class="small-genres">ACTION, SUPRANATURAL</div>
                                    <h1 class="main-title">CHAINSAW MAN</h1>
                                    <h3 class="sub-title">THE SERIES THAT MOST POPULAR LAST YEAR, 2nd SESSION FINALY SETTELED</h3>
                                    <p>Because he was in debt he was forced to become a demon exterminator. But instead he turned into a half devil. What will happen next?</p>
                                    <a class="btn btn-danger no-round" data-bs-toggle="modal" data-bs-target="#trailer_modal" onclick="opentrailer('https\:\/\/www.youtube.com/embed/v4yLeNt-kCU?si=WawG_E3zm4KOS_uj')">
                                        <i class="fa fa-youtube"></i> Watch Trailer
                                    </a>
                                </div>
                            </div>
                            
                            <div class="row d-block d-sm-none d-md-none d-lg-none">
                                <div class="col-12 px-5">
                                    <a class="btn btn-dark no-round w-100" type="button" data-bs-target="#home_carousel" data-bs-slide="prev">
                                        <i class="fa fa-chevron-left"></i> Frieren, Beyond Journey's End
                                    </a>
                                </div>
                                <div class="col-12 px-5">
                                    <a class="btn btn-primary no-round w-100 text-left" type="button" data-bs-target="#home_carousel" data-bs-slide="next">
                                        I Got Reincarnated as a Slime <i class="fa fa-chevron-right"></i> 
                                    </a>
                                </div>
                            </div>
                            <div class="row d-none d-sm-block d-md-block d-lg-block">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-5">
                                    <div class="row">
                                        <div class="col-6 col-md-5">
                                            <a class="back-action d-block" style="background-image: url('assets/img/frieren-small.jpeg');" data-bs-target="#home_carousel" data-bs-slide="prev">
                                                <div class="row dark-overlay py-3">
                                                    <div class="col-2 carousel-action-icon">
                                                        <i class="fa fa-chevron-left"></i>
                                                    </div>
                                                    <div class="col-10 text-left carousel-action-text pr-2">
                                                        <span>Frieren, Beyond Journey's End</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-5">
                                            <a class="back-action d-block" style="background-image: url('assets/img/rimuru-el-slilme-small.jpeg');" data-bs-target="#home_carousel" data-bs-slide="next">
                                                <div class="row blue-overlay py-3">
                                                    <div class="col-10 carousel-action-text pl-2">
                                                        <span class="text-left pr-0 pl-2">I Got Reincarnated as a Slime</span>
                                                    </div>
                                                    <div class="col-2 carousel-action-icon">
                                                        <i class="fa fa-chevron-right"></i>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    </div>
                <!-- cainsawman end -->
                <!-- slime start -->
                <div class="carousel-item">
                    <div class="main-banner-item bg-secondary" style="background-image: url('assets/img/banner-rimuru.jpeg');">
                        <div class="container white-gradient-left">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-5">
                                    <div class="small-genres">ACTION, ISEKAI</div>
                                    <h1 class="main-title">THAT TIME I GOT REINCARNATED AS A SLIME</h1>
                                    <h3 class="sub-title">THE 3rd WAS RELEASE TODAY</h3>
                                    <p>The conflict with the heavy church is getting hotter. Will Rimuru go to war with Hinata?</p>
                                    <a class="btn btn-danger no-round" data-bs-toggle="modal" data-bs-target="#trailer_modal" onclick="opentrailer('https\:\/\/www.youtube.com/embed/oxZWA16zjO0?si=OAgn6zzuMFYkbRoN')">
                                        <i class="fa fa-youtube"></i> Watch Trailer
                                    </a>
                                </div>
                            </div>
                            
                            <div class="row d-block d-sm-none d-md-none d-lg-none">
                                <div class="col-12 px-5">
                                    <a class="btn btn-dark no-round w-100" type="button" data-bs-target="#home_carousel" data-bs-slide="prev">
                                        <i class="fa fa-chevron-left"></i> Chainsaw Man 
                                    </a>
                                </div>
                                <div class="col-12 px-5">
                                    <a class="btn btn-primary no-round w-100 text-left" type="button" data-bs-target="#home_carousel" data-bs-slide="next">
                                        Frieren, Beyond Journey's End <i class="fa fa-chevron-right"></i> 
                                    </a>
                                </div>
                            </div>
                            <div class="row d-none d-sm-block d-md-block d-lg-block">
                                <div class="col-12 col-sm-12 col-md-12 col-lg-6 p-5">
                                    <div class="row">
                                        <div class="col-6 col-md-5">
                                            <a class="back-action d-block" style="background-image: url('assets/img/cain-saw-man-small.jpeg');" data-bs-target="#home_carousel" data-bs-slide="prev">
                                                <div class="row dark-overlay py-3">
                                                    <div class="col-2 carousel-action-icon">
                                                        <i class="fa fa-chevron-left"></i>
                                                    </div>
                                                    <div class="col-10 text-left carousel-action-text pr-2">
                                                        <span>Chainsaw Man<br>チェンソーマン</span>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                        <div class="col-6 col-md-5">
                                            <a class="back-action d-block" style="background-image: url('assets/img/frieren-small.jpeg');" data-bs-target="#home_carousel" data-bs-slide="next">
                                                <div class="row blue-overlay py-3">
                                                    <div class="col-10 carousel-action-text pl-2">
                                                        <span class="text-left pr-0 pl-2">Frieren, Beyond Journey's End</span>
                                                    </div>
                                                    <div class="col-2 carousel-action-icon">
                                                        <i class="fa fa-chevron-right"></i>
                                                    </div>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    </div>
                <!-- slime end -->
                </div>
            </div>
        </div>
    </div>
</section>

<section class="container featured-anime bg-light px-0">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-8 col-lg-8">
            <div class="best-section-container bg-light">
                <div class="row">
                    <div class="col-12">
                        <div class="featured-category-container mb-4 pt-3">
                            <h2 class="the-best-title" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="100">
                                <span class="pt-3">The Best of</span>
                            </h2>
                            <ul class="the-best-of-category">
                                <li>
                                    <a href="<?= base_url('anime') ?>?premiered_at=spring 2024" class="btn btn-sm btn-primary no-round" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="100">Spring 2024</a>
                                </li>
                                <li>
                                    <a href="<?= base_url('anime') ?>?premiered_at=winter 2023" class="btn btn-sm btn-outline-primary no-round" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="200">Winter 2023</a>
                                </li>
                                <li>
                                    <a href="<?= base_url('anime') ?>?premiered_at=fall 2023" class="btn btn-sm btn-outline-primary no-round" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="300">Fall 2023</a>
                                </li>
                                <li>
                                    <a href="<?= base_url('anime') ?>?premiered_at=summer 2023" class="btn btn-sm btn-outline-primary no-round" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="400">Summer 2023</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <?php foreach ($best_anime as $anime) : ?>
                        <div class="col-12 col-sm-12 col-md-6 col-lg-3">
                            <?= view('anime/includes/vertical-anime-card', $anime) ?>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
            <div class="latest-episodes-container bg-light px-5 px-sm-5 px-md-1 px-lg-1">
                <div class="row">
                    <div class="col-12">
                        <h2 data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="900">Lates Episodes</h2>
                    </div>
                </div>
                <div class="row small-horizontal-card" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="1000">
                    <div class="col-4">
                        <a href="anime-detail.html"><img src="assets/img/frieren-small.jpeg" alt="card title"></a>
                    </div>
                    <div class="col-8">
                        <h1><a href="anime-detail.html">Sosou no Frieren</a></h1>
                        <h3>Episode 27, Fighting Double</h3>
                        <span>1,012 views</span>
                    </div>
                </div>
                <div class="row small-horizontal-card" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="1100">
                    <div class="col-4">
                        <a href="anime-detail.html"><img src="assets/img/frieren-small.jpeg" alt="card title"></a>
                    </div>
                    <div class="col-8">
                        <h1><a href="anime-detail.html">Sosou no Frieren</a></h1>
                        <h3>Episode 27, Fighting Double</h3>
                        <span>1,012 views</span>
                    </div>
                </div>
                <div class="row small-horizontal-card" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="1200">
                    <div class="col-4">
                        <a href="anime-detail.html"><img src="assets/img/frieren-small.jpeg" alt="card title"></a>
                    </div>
                    <div class="col-8">
                        <h1><a href="anime-detail.html">Sosou no Frieren</a></h1>
                        <h3>Episode 27, Fighting Double</h3>
                        <span>1,012 views</span>
                    </div>
                </div>
                <div class="row small-horizontal-card" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="1300">
                    <div class="col-4">
                        <a href="anime-detail.html"><img src="assets/img/frieren-small.jpeg" alt="card title"></a>
                    </div>
                    <div class="col-8">
                        <h1><a href="anime-detail.html">Sosou no Frieren</a></h1>
                        <h3>Episode 27, Fighting Double</h3>
                        <span>1,012 views</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="container new-and-trailer-container bg-light px-0 pt-3 pb-4">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-8 col-lg-8">
            <div class="new-container px-5">
                <div class="row">
                    <div class="col-9">
                        <h2 data-aos="fade-left" data-aos-duration="4000" data-aos-delay="100">Recent News</h2>
                    </div>
                    <div class="col-3 text-right">
                        <a href="news.html" class="btn btn-sm btn-outline-primary no-round" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="200">View All</a>
                    </div>
                </div>
                <div class="row big-horizontal-card" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="300">
                        <div class="col-4">
                        <img src="assets/img/frieren-list-small.png" alt="News Title Here">
                        </div>
                        <div class="col-8">
                        <small>Posted on 13/08/2024</small>
                        <h3>Release Date for Overlord The Movie Was Confirmed</h3>
                        <p>Through a brand-new trailer released today, the powers that be have designated a Fall 2024 release date for upcoming anime film Overlord: The Sacred Kingdom and .... </p>
                        <a href="news-detail.html" class="text-primary">Continue reading...</a>
                        <a href="news-detail.html" class="text-secondary float-right"><i class="fa fa-comment"></i> 54 Comments</a>
                        </div>
                </div>
                <div class="row big-horizontal-card" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="400">
                    <div class="col-4">
                        <img src="assets/img/frieren-list-small.png" alt="News Title Here">
                    </div>
                    <div class="col-8">
                        <small>Posted on 13/08/2024</small>
                        <h3>Release Date for Overlord The Movie Was Confirmed</h3>
                        <p>Through a brand-new trailer released today, the powers that be have designated a Fall 2024 release date for upcoming anime film Overlord: The Sacred Kingdom and .... </p>
                        <a href="news-detail.html" class="text-primary">Continue reading...</a>
                        <a href="news-detail.html" class="text-secondary float-right"><i class="fa fa-comment"></i> 54 Comments</a>
                    </div>
                </div>
                <div class="row big-horizontal-card" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="500">
                    <div class="col-4">
                        <img src="assets/img/frieren-list-small.png" alt="News Title Here">
                    </div>
                    <div class="col-8">
                        <small>Posted on 13/08/2024</small>
                        <h3>Release Date for Overlord The Movie Was Confirmed</h3>
                        <p>Through a brand-new trailer released today, the powers that be have designated a Fall 2024 release date for upcoming anime film Overlord: The Sacred Kingdom and .... </p>
                        <a href="news-detail.html" class="text-primary">Continue reading...</a>
                        <a href="news-detail.html" class="text-secondary float-right"><i class="fa fa-comment"></i> 54 Comments</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-12 col-md-4 col-lg-4">
            <div class="latest-trailer-container px-5 px-sm-5 px-md-0 px-md-0">
                <div class="row">
                    <div class="col-7 col-sm-7 col-md-6 col-lg-6">
                        <h2 data-aos="fade-up" data-aos-duration="4000" data-aos-delay="100">Recent Trailer</h2>
                    </div>
                    <div class="col-5 col-sm-5 col-md-6 col-md-6 px-0 px-sm-0 px-md-5 px-md-5">
                        <a href="trailer.html" class="btn btn-sm btn-outline-primary float-right no-round" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="100">View More</a>
                    </div>
                </div>
                <div class="row small-horizontal-card-video" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="100">
                    <div class="col-5">
                        <a class="thumb d-block" href="anime-detail.html" style="background-image: url('assets/img/frieren-list-small.png');">
                            <img src="assets/img/youtube-play-transparent.png" alt="trailer card">
                        </a>
                    </div>
                    <div class="col-7">
                        <a href="anime-detail.html"><h3>Sosou no Frieren</h3></a>
                        <p>Episode 27, Fighting Double</p>
                        <span>1,012 views</span>
                        <a href="anime-detail.html" class="text-primary">24 Comments</a>
                    </div>
                </div>
                <div class="row small-horizontal-card-video" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="200">
                    <div class="col-5">
                        <a class="thumb d-block" href="anime-detail.html" style="background-image: url('assets/img/frieren-list-small.png');">
                            <img src="assets/img/youtube-play-transparent.png" alt="trailer card">
                        </a>
                    </div>
                    <div class="col-7">
                        <a href="anime-detail.html"><h3>Sosou no Frieren</h3></a>
                        <p>Episode 27, Fighting Double</p>
                        <span>1,012 views</span>
                        <a href="anime-detail.html" class="text-primary">24 Comments</a>
                    </div>
                </div>
                <div class="row small-horizontal-card-video" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="300">
                    <div class="col-5">
                        <a class="thumb d-block" href="anime-detail.html" style="background-image: url('assets/img/frieren-list-small.png');">
                            <img src="assets/img/youtube-play-transparent.png" alt="trailer card">
                        </a>
                    </div>
                    <div class="col-7">
                        <a href="anime-detail.html"><h3>Sosou no Frieren</h3></a>
                        <p>Episode 27, Fighting Double</p>
                        <span>1,012 views</span>
                        <a href="anime-detail.html" class="text-primary">24 Comments</a>
                    </div>
                </div>
                <div class="row small-horizontal-card-video" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="400">
                    <div class="col-5">
                        <a class="thumb d-block" href="anime-detail.html" style="background-image: url('assets/img/frieren-list-small.png');">
                            <img src="assets/img/youtube-play-transparent.png" alt="trailer card">
                        </a>
                    </div>
                    <div class="col-7">
                        <a href="anime-detail.html"><h3>Sosou no Frieren</h3></a>
                        <p>Episode 27, Fighting Double</p>
                        <span>1,012 views</span>
                        <a href="anime-detail.html" class="text-primary">24 Comments</a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</section>
<?= $this->endSection() ?>