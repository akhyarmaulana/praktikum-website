<?= $this->extend('anime/includes/template') ?>

<?= $this->section('content') ?>

<section class="container detail-anime bg-light px-0 pt-2">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-4 col-lg-4 px-5">
                <img src="<?= base_url($data->featured_image) ?>" class="w-100 main-poster" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="500">

                <div class="row">
                    <div class="col-12">
                        <h4 class="mt-4" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="600">Information</h4>
                        <hr data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="600">
                        <ul class="info-points" data-aos="fade-up" ata-aos-duration="4000" data-aos-delay="700">
                            <li>Episodes : <?= $data->total_episode ?></li>
                            <li>Premiered : <a href="#"><?= ucfirst($data->premiered_at) ?></a></li>
                            <li>Producers :  <?= $data->producers ?></li>
                            <li>Studios :  <?= $data->studios ?></li>
                            <li>Genres :  <?= $data->genres ?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-12 col-sm-12 col-md-8 col-lg-8 px-5">
                <div class="row pb-3">
                    <div class="col-12">
                        <h1 data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="500"><?=$data->title?></h1>
                        <hr data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="500">
                    </div>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 text-center rating-col" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="600">
                        <span class="score-label">SCORE</span>
                        <p class="star-score-avg"><?= rand(5,10)/2 ?>/5</p>
                        <small><?= number_format(rand(11111,99999), 0, ",", ".") ?> users</small>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 text-center" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="700">
                        <span class="score-label">RANKED</span>
                        <p class="star-score-avg">#<?= rand(1,10)/2 ?></p>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 text-center" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="800">
                        <span class="score-label">POPULARITY</span>
                        <p class="star-score-avg">#<?= rand(111,999) ?></p>
                    </div>
                    <div class="col-6 col-sm-6 col-md-3 col-lg-3 text-center" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="900">
                        <span class="score-label">MEMBER</span>
                        <p class="star-score-avg"><?= number_format(rand(11111,99999), 0, ",", ".") ?></p>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-12">
                        <h4 data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="500">Synopsis</h4>
                        <hr data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="500">
                        <p data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="600">
                            <?=$data->sinopsis?>
                        </p>
                        <!-- <p data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="700">However, the time that Frieren spends with her comrades is equivalent to merely a fraction of her life, which has lasted over a thousand years. When the party disbands after their victory, Frieren casually returns to her "usual" routine of collecting spells across the continent. Due to her different sense of time, she seemingly holds no strong feelings toward the experiences she went through.</p>
                        <p data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="800">As the years pass, Frieren gradually realizes how her days in the hero's party truly impacted her. Witnessing the deaths of two of her former companions, Frieren begins to regret having taken their presence for granted; she vows to better understand humans and create real personal connections. Although the story of that once memorable journey has long ended, a new tale is about to begin.</p> -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <h4 data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="500">Review</h4>
                        <hr data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="500">
                        <div class="anime-review-card" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="600">
                            <div class="row">
                                <div class="col-1">
                                    <img src="assets/img/onizuka-pp.jpg" alt="User picture" class="w-100">
                                </div>
                                <div class="col-3">
                                    <h4>Onizuka Sensei</h4>
                                    <div class="rating">
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star inactive-star"></i>
                                        <i class="fa fa-star inactive-star"></i>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <span class="time-ago">
                                        32 minutes ago
                                    </span>
                                </div>
                                <div class="col-12 py-2">
                                    <p>With lives so short, why do we even bother? To someone who lives for thousands of years, it is barely worth giving the wistful existence of humans much consideration, let alone getting to know them. That is, at least, until you meet one who is worth getting to know.</p>
                                    <p>From the very outset, this series does not hide that it will focus on some of the tough questions in life: purpose, meaning, our own endings. Yet embedded within these, it unpacks the many guised forms of regret. Upfront it reminds us that our lives are short; our prime is even shorter. It is easy to be ...</p>
                                    <hr>
                                </div>
                                <div class="col-3">
                                    <a href="#" class="btn btn-outline-primary btn-sm no-round"><i class="fa fa-reply"></i> Reply this review</a>
                                </div>
                            </div>
                        </div>
                        <div class="anime-review-card" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="700">
                            <div class="row">
                                <div class="col-1">
                                    <img src="assets/img/onizuka-pp.jpg" alt="User picture" class="w-100">
                                </div>
                                <div class="col-3">
                                    <h4>Onizuka Sensei</h4>
                                    <div class="rating">
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star inactive-star"></i>
                                        <i class="fa fa-star inactive-star"></i>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <span class="time-ago">
                                        32 minutes ago
                                    </span>
                                </div>
                                <div class="col-12 py-2">
                                    <p>With lives so short, why do we even bother? To someone who lives for thousands of years, it is barely worth giving the wistful existence of humans much consideration, let alone getting to know them. That is, at least, until you meet one who is worth getting to know.</p>
                                    <p>From the very outset, this series does not hide that it will focus on some of the tough questions in life: purpose, meaning, our own endings. Yet embedded within these, it unpacks the many guised forms of regret. Upfront it reminds us that our lives are short; our prime is even shorter. It is easy to be ...</p>
                                    <hr>
                                </div>
                                <div class="col-3">
                                    <a href="#" class="btn btn-outline-primary btn-sm no-round"><i class="fa fa-reply"></i> Reply this review</a>
                                </div>
                            </div>
                        </div>
                        <div class="anime-review-card" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="800">
                            <div class="row">
                                <div class="col-1">
                                    <img src="assets/img/onizuka-pp.jpg" alt="User picture" class="w-100">
                                </div>
                                <div class="col-3">
                                    <h4>Onizuka Sensei</h4>
                                    <div class="rating">
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star inactive-star"></i>
                                        <i class="fa fa-star inactive-star"></i>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <span class="time-ago">
                                        32 minutes ago
                                    </span>
                                </div>
                                <div class="col-12 py-2">
                                    <p>With lives so short, why do we even bother? To someone who lives for thousands of years, it is barely worth giving the wistful existence of humans much consideration, let alone getting to know them. That is, at least, until you meet one who is worth getting to know.</p>
                                    <p>From the very outset, this series does not hide that it will focus on some of the tough questions in life: purpose, meaning, our own endings. Yet embedded within these, it unpacks the many guised forms of regret. Upfront it reminds us that our lives are short; our prime is even shorter. It is easy to be ...</p>
                                    <hr>
                                </div>
                                <div class="col-3">
                                    <a href="#" class="btn btn-outline-primary btn-sm no-round"><i class="fa fa-reply"></i> Reply this review</a>
                                </div>
                            </div>
                        </div>
                        <div class="anime-review-card" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="900">
                            <div class="row">
                                <div class="col-1">
                                    <img src="assets/img/onizuka-pp.jpg" alt="User picture" class="w-100">
                                </div>
                                <div class="col-3">
                                    <h4>Onizuka Sensei</h4>
                                    <div class="rating">
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star active-star"></i>
                                        <i class="fa fa-star inactive-star"></i>
                                        <i class="fa fa-star inactive-star"></i>
                                    </div>
                                </div>
                                <div class="col-8">
                                    <span class="time-ago">
                                        32 minutes ago
                                    </span>
                                </div>
                                <div class="col-12 py-2">
                                    <p>With lives so short, why do we even bother? To someone who lives for thousands of years, it is barely worth giving the wistful existence of humans much consideration, let alone getting to know them. That is, at least, until you meet one who is worth getting to know.</p>
                                    <p>From the very outset, this series does not hide that it will focus on some of the tough questions in life: purpose, meaning, our own endings. Yet embedded within these, it unpacks the many guised forms of regret. Upfront it reminds us that our lives are short; our prime is even shorter. It is easy to be ...</p>
                                    <hr>
                                </div>
                                <div class="col-3">
                                    <a href="#" class="btn btn-outline-primary btn-sm no-round"><i class="fa fa-reply"></i> Reply this review</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?= $this->endSection() ?>