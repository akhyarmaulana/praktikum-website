<?= $this->extend('anime/includes/template') ?>

<?= $this->section('content') ?>

<section class="container featured-anime bg-light px-0">
    <div class="row">
        <div class="col-12 p-5">

            <div class="row justify-content-center">
                <div class="col-12 col-md-6">
                    <h1 class="mt-5">Login Form</h1>
                    Silahkan login untuk berpartisipasi aktif
                    <hr />
                    <?php if (!empty(session()->getFlashdata('error'))) : ?>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <?php echo session()->getFlashdata('error'); ?>
                        </div>
                    <?php endif; ?>
                    <form method="post" action="<?= base_url(); ?>login">
                        <?= csrf_field(); ?>
                        <!-- <input type="text" name="email" id="email" placeholder="Email" class="form-control" required autofocus> -->
                        <div class="mb-3">
                            <label for="email" class="form-label">Email</label>
                            <input type="text" class="form-control" id="email" name="email">
                        </div>
                        <div class="mb-3">
                            <label for="password" class="form-label">Password</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <!-- <input type="password" name="password" id="password" placeholder="Password" class="form-control" required> -->
                        <div class="mb-3">
                            <button type="submit" class="w-100 btn btn-lg btn-primary">Login</button>
                        </div>
                        <!-- <p class="mt-5 mb-3 text-muted">&copy; Rumah Bimbel</p> -->
                    </form>
                    
                </div>
            </div>
            

        </div>
    </div>
</section>

<?= $this->endSection() ?>