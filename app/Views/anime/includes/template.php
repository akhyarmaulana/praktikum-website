<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Anime Net</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/css/style.css">
    
    <link rel="apple-touch-icon" href="favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=base_url()?>favicon/favicon-16x16.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=base_url()?>favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?=base_url()?>favicon/android-chrome-192x192.png">
    <link rel="icon" type="image/png" sizes="512x512" href="<?=base_url()?>favicon/android-chrome-512x512.png">
    
  </head>
  <body>

  <?= $this->include('anime/includes/header') ?>

  <?= $this->renderSection('content') ?>

  <?= $this->include('anime/includes/footer') ?>

  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
        var home_carousel = document.querySelector('#home_carousel')
        var carousel = new bootstrap.Carousel(home_carousel, {
            interval: 6000,
        })
    </script>
    <script>
        function opentrailer(youtubelink)
        {
            html = `<iframe width="100%" height="450" src="`+youtubelink+`" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>`;
            $('#main-embed-body').html(html);
        }
    </script>
    <script>
        AOS.init();
    </script>
    <script>
        $("#search-action").click(function(){
            window.location = "animes.html";
        })
    </script>
  </body>
</html>