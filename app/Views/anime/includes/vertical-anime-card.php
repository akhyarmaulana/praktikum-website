<div class="small-vertical-card" data-aos="fade-left" ata-aos-duration="4000" data-aos-delay="700">
    <div class="row">
        <div class="col-12">
            <a href="<?=base_url('anime/detail/' . $slug)?>"><img src="<?=base_url($featured_image)?>"></a>
            <small><?=$genres?></small>
            <h1><a href="anime-detail.html"><?=$title?></a></h1>
        </div>
        <div class="col-12">
            <?php $active_start = rand(1,5); ?>
            <?php for ($i = 0; $i < 5; $i++) : ?>
            <i class="fa fa-star <?= $i <= $active_start ? "active-star" : "" ?>" ></i>
            <?php endfor ?>
        </div>
        <div class="col-12 mb-2">
            <span>(<?=rand(111,999)?> reviews)</span>
        </div>
        <div class="col-6 watching-box">
            <div class="w-100 bg-primary py-1">
                <h4><?=rand(111,999)?></h4>
                <p>Watching</p>
            </div>
        </div>
        <div class="col-6 episode-box">
            <div class="w-100 d-block bg-secondary py-1">
                <h4><?=$total_episode?></h4>
                <p>Episode</p>
            </div>
        </div>
    </div>
</div>