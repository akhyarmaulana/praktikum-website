<header class="container px-0">
    <nav class="navbar navbar-expand-lg p-0">
        <div class="container bg-light blue-line-bottom pl-0 nav-container">
            <a class="navbar-brand p-0" href="#" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="300">
                <div class="main-logo">
                    <img src="<?=base_url()?>assets/img/logo.png" alt="Anime Net Logo">
                </div>
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#main-nav" aria-controls="main-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse pb-3 pt-3" id="main-nav">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item" data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="500">
                    <a class="nav-link active" aria-current="page" href="<?=base_url()?>">Home</a>
                </li>
                <li class="nav-item" data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="600">
                    <a class="nav-link " aria-current="page" href="<?=base_url('anime')?>">Animes</a>
                </li>
                <li class="nav-item" data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="800">
                    <a class="nav-link" aria-current="page" href="<?=base_url('news')?>">News</a>
                </li>
                <li class="nav-item" data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="900">
                    <a class="nav-link" aria-current="page" href="<?=base_url('genre')?>">Genres</a>
                </li>
                
                <?php if (true) : ?>
                    <?php if (session()->get('id')) : ?>
                    <li class="nav-item" data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="900">
                        <a class="nav-link btn" style="color:#1464e9" aria-current="page" href="<?=base_url('profile')?>">Hi, <?=session()->get('name')?> </a>
                    </li>
                    <?php else : ?>
                    <li class="nav-item" data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="900">
                        <a class="nav-link" aria-current="page" href="<?=base_url('register')?>">Register</a>
                    </li>
                    <li class="nav-item" data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="900">
                        <a class="nav-link" aria-current="page" href="<?=base_url('login')?>">Login</a>
                    </li>
                    <?php endif; ?>
                <?php endif; ?>
            </ul>
            <form action="<?=base_url('anime')?>" class="d-flex nav-search pl-3" role="search" data-aos="fade-left" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1000">
                <div class="input-group">
                    <input name="search" type="text" class="form-control no-round" placeholder="Search anime" aria-label="Recipient's username" aria-describedby="search-action" value="<?= !empty($_GET['search']) ? $_GET['search'] : "" ?>">
                    <button class="btn btn-primary no-round" type="submit" id="search-action"><i class="fa fa-search"></i> Search</button>
                </div>
            </form>
            </div>
        </div>
    </nav>
</header>