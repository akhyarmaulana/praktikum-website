<footer class="container px-0" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="500">
    <div class="container footer-layer pt-5 pb-2">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-3 col-lg-3 px-5">
                <img src="<?=base_url('assets/img/logo-negative.png')?>" alt="Main Logo" class="w-100 mb-4 mb-sm-4 mb-md-0 mb-lg-0" >
            </div>
            <div class="col-12 col-sm-12 col-md-9 col-lg-9 px-5">
                <ul class="footer-nav">
                    <li>
                        <a href="<?=base_url('anime')?>" >Animes</a>
                    </li>
                    <li>
                        <a href="<?=base_url('news')?>" >News</a>
                    </li>
                    <li>
                        <a href="<?=base_url('genres')?>" >Genres</a>
                    </li>
                    
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-12 px-5 pt-4">
                <p class="text-white text-center mb-0">Copyright © 2024 Anime Net. All rights reserved</p>
            </div>
        </div>
    </div>
</footer>