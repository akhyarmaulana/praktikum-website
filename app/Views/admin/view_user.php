<div class="card">
    <div class="card-header">
        <button type="button" class="btn btn-primary float-right" data-bs-toggle="modal" data-bs-target="#exampleModal">
            Tambah Data
        </button>
    </div>
    <!-- /.card-header -->

    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Password</th>
                    <th>Name</th>
                    <th>Access Rule</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; foreach ($user as $us) : ?>
                <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $us->username; ?></td>
                    <td><?= $us->email; ?></td>
                    <td><?= $us->password; ?></td>
                    <td><?= $us->name; ?></td>
                    <td><?= $us->access_rule; ?></td>
                    <td>
                        <button type="button" class="badge badge-primary" data-bs-toggle="modal" data-bs-target="#editModal<?= $us->id; ?>">
                            Edit
                        </button>
                        <a href="#" class="badge badge-danger">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
