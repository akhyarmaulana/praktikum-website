<div class="card">
    <div class="card-header">
        <a type="button" class="btn btn-primary" href="<?=base_url('admin/'.$module.'')?>">
             <i class="fa fa-chevron-left"></i> Lihat Table
        </a>
    </div>
    <!-- /.card-header -->

    <div class="card-body">
        <?php if (!empty(session()->getFlashdata('error'))) : ?>
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <h4>Periksa Entrian Form</h4>
                <?php echo session()->getFlashdata('error'); ?>
            </div>
        <?php endif; ?>

        <form method="post" action="<?= base_url('admin/' . $module . '/'  . $mode . '/' . $reff_id); ?>" enctype='multipart/form-data'>
            <?= csrf_field(); ?>

            <?php foreach ($form as $f) : ?>

                <?php if ($f['type'] == 'text') : ?>
                    <div class="mb-3">
                        <label for="<?=$f['name']?>" class="form-label"><?=$f['label']?></label>
                        <input type="text" class="form-control" id="<?=$f['name']?>" name="<?=$f['name']?>" value="<?= !empty($data[$f['name']]) ? $data[$f['name']] : "" ?>">
                    </div>
                <?php endif ?>

                <?php if ($f['type'] == 'number') : ?>
                    <div class="mb-3">
                        <label for="<?=$f['name']?>" class="form-label"><?=$f['label']?></label>
                        <input type="number" class="form-control" id="<?=$f['name']?>" name="<?=$f['name']?>" value="<?= !empty($data[$f['name']]) ? $data[$f['name']] : "" ?>">
                    </div>
                <?php endif ?>

                <?php if ($f['type'] == 'textarea') : ?>
                    <div class="mb-3">
                        <label for="<?=$f['name']?>" class="form-label"><?=$f['label']?></label>
                        <textarea type="text" class="form-control" id="<?=$f['name']?>" name="<?=$f['name']?>"><?= !empty($data[$f['name']]) ? $data[$f['name']] : "" ?></textarea>
                    </div>
                <?php endif ?>

                <?php if ($f['type'] == 'select') : ?>
                    <div class="mb-3">
                        <label for="<?=$f['name']?>" class="form-label"><?=$f['label']?></label>
                        
                        <select name="<?=$f['name']?>" id="<?=$f['name']?>" class="form-control">
                            <option value="">-Select Options-</option>
                            <?php foreach ($f['options'] as $opt) : ?>
                                <option value="<?=$opt['value']?>" <?= !empty($data[$f['name']]) && $opt['value'] == $data[$f['name']] ? "selected" : "" ?>><?=$opt['label']?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                <?php endif ?>

                <?php if ($f['type'] == 'file') : ?>
                    <div class="mb-3">
                        <label for="<?=$f['name']?>" class="form-label"><?=$f['label']?></label>
                        <input type="file" class="form-control" id="<?=$f['name']?>" name="<?=$f['name']?>">
                    </div>
                <?php endif ?>


                <?php if ($f['type'] == 'summernote') : ?>
                    <div class="mb-3">
                        <label for="<?=$f['name']?>" class="form-label"><?=$f['label']?></label>
                        <?php if (!empty($f['function_get'])) : ?>
                        <textarea id="summernote" name="<?=$f['name']?>"><?= !empty($data[$f['name']]) ? $f['function_get']($data[$f['name']]) : "" ?></textarea>
                        <?php else : ?>
                        <textarea id="summernote" name="<?=$f['name']?>"><?= !empty($data[$f['name']]) ? $data[$f['name']] : "" ?></textarea>
                        <?php endif; ?>
                    </div>
                <?php endif ?>

            <?php endforeach ?>
            
            
            <div class="mb-3">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
