<div class="card">
    <div class="card-header">
        <a type="button" class="btn btn-primary float-right" href="<?=base_url('admin/'.$module.'/add')?>">
            Tambah Data
        </a>
    </div>
    <!-- /.card-header -->

    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <?php foreach ($table as $v) : ?>
                    <th><?=$v['label']?></th>
                    <?php endforeach; ?>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; foreach ($data as $v) : ?>
                <tr>
                    <td><?= $i++; ?></td>
                    <?php foreach ($table as $t) : ?>
                        <?php if (!empty($t['image']) && $t['image'] == true && !empty($v[$t['name']])) : ?>
                            <td>
                                <a href="<?=base_url($v[$t['name']])?>" target="_blank">
                                    <img src="<?=base_url($v[$t['name']])?>" style="width:60px;height:60px;border-radius:7px;object-fit:cover">
                                </a>
                            </td>
                        <?php else : ?>
                            <td><?= $v[$t['name']] ?></td>
                        <?php endif ?>
                        
                    <?php endforeach ?>
                    <td>
                        <a href="<?=base_url('admin/'.$module.'/edit/' . $v['id'])?>" class="badge badge-primary">Edit</a>
                        <a href="<?=base_url('admin/'.$module.'/delete/' . $v['id'])?>" class="badge badge-danger">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
</div>
<!-- /.card -->
