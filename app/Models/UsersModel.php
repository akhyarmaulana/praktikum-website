<?php
namespace App\Models;
use CodeIgniter\Model;

class UsersModel extends Model
{
    protected $table = "users";
    protected $primaryKey = "id";
    protected $returnType = "array";
    protected $useTimestamps = true;
    protected $allowedFields = ['username', 'email', 'password', 'name', 'access_rule'];

    public function get_user()
    {
        return $this->findAll();
    }

}